# -*- coding: utf-8 -*-
r''' Script downloads image from bing.com and sets it as background
(working for gnome in ubuntu 10.04+). If no image is found, then 
randomly picked image from picture directory is sets as background.
\version v20111027
\author Adam Hlavatovič '''
import os, subprocess, random, urllib, re, argparse


def main(args):
	plink = picture_link()
	pdir = os.path.expanduser(args.pictures)
	ppath = os.path.join(pdir, extract_picture_name(plink))
	if not os.path.exists(ppath):
		download_picture(plink, ppath)
	else:
		ppath = os.path.join(pdir, random_picture(pdir))
	set_as_background(ppath)

def picture_link():
	expr = r".*g_img=\{url:'([^']+)'.*"
	bing_page = urllib.urlopen('http://www.bing.com')
	return 'http://www.bing.com%s' % ( 
		re.search(expr, bing_page.read()).group(1),)

def download_picture(link, copy_to):	
	urllib.urlretrieve(link, copy_to)

def random_picture(pictures):
	r'\return Randomly picked picture name.'
	files = os.listdir(pictures)
	return random.choice(files)

def set_as_background(picture):
	subprocess.call(['gconftool-2', '-s', '-t', 'string', 
		'/desktop/gnome/background/picture_filename', '%s' % (picture,)])
	subprocess.call(['gconftool-2', '-s', '-t', 'string', 
		'/desktop/gnome/background/picture_options', 'stretched'])

def extract_picture_name(link):
	return os.path.basename(link)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='PyBing')
	parser.add_argument('-p', dest='pictures', default='~/Pictures')
	args = parser.parse_args()
	main(args)
