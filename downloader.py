# -*- coding: utf-8 -*-
# Downloads wallpaper images from various sources (curently 
# national-geographic and microsoft-bing).
# Run
#    $ python downloader.py -h 
# for more help.
import sys, os, re, urllib, argparse


def main(args):
	downloaders = [national_geographic(), bing()]
	
	create_output_dirs_if_not_exists(downloaders, args.output_directory, 
		args.s)

	for downloader in downloaders:
		download(downloader, args.output_directory, args.n, args.s)


def download(downloader, directory, n_prev, separate):
	if separate:
		directory = os.path.join(directory, downloader.identify())

	pod = downloader.photo_of_the_day()
	save_if_not_exists(pod, directory)

	if n_prev != 0:
		prev_pods = downloader.previous_photo_of_the_day(n_prev)
		for pod in prev_pods:
			save_if_not_exists(pod, directory)


def save_if_not_exists(pod, directory):
	image_path = os.path.join(directory, pod.basename())
	if not os.path.exists(image_path):
		pod.save_as(image_path)
		print "Image '%s' saved." % image_path
	else:
		print "Image '%s' already exists." % image_path


def parse_args(argv):
	parser = argparse.ArgumentParser(prog='wallpaper-downloader')

	parser.add_argument(
		'output_directory', nargs='?', default=os.path.expanduser('~/Pictures'),
		help='output directiory for photos (default=~/Pictures)')

	parser.add_argument('-n', nargs='?', type=int, default=0,
		help='number of previous photos-of-the-day to download')

	parser.add_argument('-s', action='store_true', 
		help='separate images from	different sources')

	return parser.parse_args()


def create_output_dirs_if_not_exists(downloaders, path, separate):
	if not separate:
		mkdirs(path)
	else:
		for downloader in downloaders:
			mkdirs(os.path.join(path, downloader.identify()))


def mkdirs(path):
	if not os.path.exists(path):
		os.makedirs(path, 0775)


class downloader_base:
	def __init__(self):
		pass

	def photo_of_the_day(self):
		pass

	def previous_photo_of_the_day(self, n):
		return []

	def identify(self):
		return 'generic-base'


class url_image:
	def __init__(self, url):
		self._url = url

	def url(self):
		return self._url

	def basename(self):
		return os.path.basename(urllib.unquote(self._url))

	def save(self, directory):
		self.save_as(os.path.join(directory, self.basename()))

	def save_as(self, fname):
		urllib.urlretrieve(self._url, fname)


class national_geographic(downloader_base):
	MAIN_URL = 'http://photography.nationalgeographic.com'
	POD_PAGE = \
		'http://photography.nationalgeographic.com/photography/photo-of-the-day'

	def __init__(self):
		pass

	def photo_of_the_day(self):
		pod_page = self._pod_page(self.POD_PAGE)
		return national_geographic_image(self._pod_image_link_and_desc(pod_page))

	def previous_photo_of_the_day(self, n):
		'\param n number of previous images.'
		images = []
		pod_page = self._pod_page(self.POD_PAGE)
		for i in range(0, n):
			prev_link = self._prev_pod_link(pod_page)
			if not prev_link:
				break
			pod_page = self._pod_page(prev_link)
			images.append(national_geographic_image(
				self._pod_image_link_and_desc(pod_page)))
		return images

	def identify(self):
		return 'national-geographic'

	def _prev_pod_link(self, pod_page):
		link = re.search(
			'<p class="prev .*?"><a href="(.*?)">&laquo; Previous</a></p>', 
				pod_page, re.DOTALL)
		if link:
			return self.MAIN_URL + link.group(1)
		else:
			return None

	def _pod_image_link_and_desc(self, pod_page):
		'Returns photo of the day link.'
		link = re.search(  # wallpaper if available
			'<div class="download_link">.*?<a href="(.*?)".*?</div>', 
				pod_page, re.DOTALL)

		if not link:
			link = re.search(  # standard photo
				'<div class="primary_photo">.*?<img src="//(.*?)".*?</div>',
					pod_page, re.DOTALL)

		assert link, 'logic error: link not found!'

		link_str = 'http://%s' % link.group(1)

		desc = re.search('<h1>(.*?)</h1>', pod_page, re.DOTALL)

		if desc:
			return (link_str, desc.group(1))
		else:
			return (link_str, None)

	def _pod_page(self, url):
		return urllib.urlopen(url).read()


class bing(downloader_base):
	MAIN_URL = 'http://www.bing.com'

	def __init__(self):
		downloader_base.__init__(self)

	def photo_of_the_day(self):
		return url_image(self._image_link())

	def identify(self):
		return 'bing'

	def _image_link(self):
		expr = r".*g_img=\{url:'([^']+)'.*"
		bing_page = urllib.urlopen(self.MAIN_URL)
		return self.MAIN_URL + '%s' % ( 
			re.search(expr, bing_page.read()).group(1),)


class national_geographic_image(url_image):
	def __init__(self, url_desc):
		url_image.__init__(self, url_desc[0])
		self._desc = url_desc[1]

	def basename(self):
		if self._desc:
			return self._desc + '.jpg'
		else:
			return url_image.basename(self)


if __name__ == '__main__':
	main(parse_args(sys.argv))

